package be.kdg.javafx;

import be.kdg.javafx.view.BirdPresenter;
import be.kdg.javafx.view.LabelView1;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import model.Bird;

public class LabelDemo extends Application {

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) {
    Bird robin = new Bird("Blue robin" , "grey, blue");

    LabelView1 view = new LabelView1();
    BirdPresenter presenter = new BirdPresenter(view, robin);
    primaryStage.setScene(new Scene(view));
    primaryStage.show();
  }
}
