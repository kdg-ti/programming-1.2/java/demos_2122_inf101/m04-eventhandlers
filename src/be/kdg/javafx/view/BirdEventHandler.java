package be.kdg.javafx.view;

import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.input.MouseEvent;
import model.Bird;
// Version 1 EventHandler is a top level class
public class BirdEventHandler implements EventHandler<MouseEvent> {

  private final LabelView1 view;
  private final Bird robin;

  public BirdEventHandler(LabelView1 view, Bird robin) {

    this.view = view;
    this.robin = robin;
  }

  @Override
  public void handle(MouseEvent mouseEvent) {
    updateView();
  }

  private void updateView() {
    view.setAlert(String.format("Bird %s spotted %d times",
      robin.getName(),
      robin.getSpotted()));
    view.showAlert();
  }
}
