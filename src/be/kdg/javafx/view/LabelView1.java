package be.kdg.javafx.view;

import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;

public class LabelView1 extends BorderPane{
  private Label label;
  private Alert alert;

  public LabelView1() {
    this.initialiseNodes();
    this.layoutNodes();
  }

  private void initialiseNodes() {
    Image imageOk = new Image("/bird.jpg");
    ImageView image = new ImageView(imageOk);
    image.setPreserveRatio(true);
    image.setFitHeight(40);
    this.label = new Label("Accept",
      image);
    alert = new Alert(Alert.AlertType.INFORMATION);
    alert.setHeaderText("Bird Alert");
  }

  private void layoutNodes() {
    this.setCenter(this.label);
    BorderPane.setMargin(this.label, new Insets(10));
  }

  protected void setAlert(String s){
    alert.setContentText(s);
  }
  protected void  showAlert(){
     alert.showAndWait();
  }
}
