package be.kdg.javafx.view;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import model.Bird;

public class BirdPresenter {
  private final LabelView1 view;
  private final Bird robin;
  // Version 2: Event Handler is an  inner class
  class InnerBirdEventHandler implements EventHandler<MouseEvent> {

    @Override
    public void handle(MouseEvent mouseEvent) {
      updateView();
    }


  }


  public BirdPresenter(LabelView1 view, Bird robin) {
    this.view = view;
    this.robin = robin;
    addEventHandlers();
  }

  private void updateView() {
    view.setAlert(String.format("Bird %s spotted %d times",
      robin.getName(),
      robin.getSpotted()));
    view.showAlert();
  }

  private void addEventHandlers() {
    // Version 2: Event Handler is an  inner class
    //  InnerBirdEventHandler handler = new InnerBirdEventHandler();

    // version 3 Event handler is an anonymous inner class
//    view.setOnMouseClicked(new EventHandler<MouseEvent>() {
//      @Override
//      public void handle(MouseEvent mouseEvent) {
//        updateView();
//      }
//    });

//    // version 4: event handler is a lambda
    view.setOnMouseClicked( mouseEvent -> {
      robin.spot();
      updateView();
    });

  }


}
