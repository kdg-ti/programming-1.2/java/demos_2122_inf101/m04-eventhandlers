package model;

public class Bird {
  private String name ;
  private String colour;
  int spotted =0;

  public Bird(String name, String colour) {
    this.name = name;
    this.colour = colour;
  }

  public String getName() {
    return name;
  }

  public void spot(){
    spotted++;
  }

  public String getColour() {
    return colour;
  }

  public int getSpotted() {
    return spotted;
  }
}
